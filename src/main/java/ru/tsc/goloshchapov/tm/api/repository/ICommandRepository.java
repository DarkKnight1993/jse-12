package ru.tsc.goloshchapov.tm.api.repository;

import ru.tsc.goloshchapov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
